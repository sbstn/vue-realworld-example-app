import { Given, Then, And } from "cypress-cucumber-preprocessor/steps";

Given("Que visito el langing-page", () => {
  cy.visit("/");
});
Then(`Debería leer {string} en el título`, title => {
  cy.title().should("include", title);
});
And(`{string} como bajada`, title => {
  cy.get("h1").should("contain", title);
});
