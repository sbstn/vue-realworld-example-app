Feature: Sistema de autenticación con email y password

  Casos de prueba para diferentes opciones de login

  Scenario: Autenticación con los datos correctos
    Given que visito el login
    And lleno el campo email con 'contacto@tutelkan.cl'
    And lleno el campo password con 'secretPwd'
    When hago click en el botón submit
    Then debería ser redirigido al Home page

  Scenario: Autenticación con los datos incorrectos
    Given que visito el login
    And lleno el campo email con 'contacto@tutelkan.cl'
    And lleno el campo password con 'secretPwd'
    When hago click en el botón submit con los datos incorrectos
    Then debería permanecer en el login